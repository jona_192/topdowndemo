﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace GameInputDemo
{
    public class CollisionObject
    {
        public short CollisionType;
        public List<Vector2> _CornerPositions = new List<Vector2>();
        public List<Vector2> _Normals = new List<Vector2>();
        private GameObject gameObject;
        private float originToCornerDistance;
        public List<Vector2> CornerPositions
        {
            get {
                _CornerPositions.Clear();
                for (int i = 0; i < 4; i++)
                {
                    _CornerPositions.Add(GetCornerPosition(i));
                }
                return _CornerPositions;
            }
            set { this._CornerPositions = value; }
        }

        public List<Vector2> Normals
        {
            get {
                _Normals.Clear();
                List<Vector2> tempNormals = new List<Vector2>();
                for(int i = 0; i < CornerPositions.Count; i++)
                {
                    if(i < CornerPositions.Count-1)
                    {
                        tempNormals.Add(new Vector2(-(CornerPositions[i + 1].Y - CornerPositions[i].Y), CornerPositions[i + 1].X - CornerPositions[i].X));
                    }
                    else
                    { 
                        tempNormals.Add(new Vector2(-(CornerPositions[0].Y - CornerPositions[i].Y), CornerPositions[0].X - CornerPositions[i].X));
                    }
                }
                _Normals = tempNormals;
                return tempNormals;
            }
            set { this._Normals = value; }
        }


        public CollisionObject(GameObject gameObject, short collisionType)
        {
            this.CollisionType = collisionType;
            this.gameObject = gameObject;
            originToCornerDistance = (float) Math.Sqrt(((gameObject.texture.Width / 2) * (gameObject.texture.Width / 2)) + ((gameObject.texture.Height / 2) * (gameObject.texture.Height / 2)));

        }

        public Vector2 GetCornerPosition(int corner)
        {
            Vector2 cornerPosition = Vector2.Zero;

            if (corner == 0)
            {
                cornerPosition = new Vector2(gameObject.Position.X - (float)(Math.Sin(MathHelper.ToRadians(45) - gameObject.Rotation) * originToCornerDistance), gameObject.Position.Y - (float)(Math.Cos(MathHelper.ToRadians(45) - gameObject.Rotation) * originToCornerDistance));
            }
            else if (corner == 1)
            {
                cornerPosition = new Vector2(gameObject.Position.X - (float)(Math.Sin(MathHelper.ToRadians(-45) - gameObject.Rotation) * originToCornerDistance), gameObject.Position.Y - (float)(Math.Cos(MathHelper.ToRadians(-45) - gameObject.Rotation) * originToCornerDistance));
            }
            else if (corner == 2)
            {
                cornerPosition = new Vector2(gameObject.Position.X + (float)(Math.Sin(MathHelper.ToRadians(45) - gameObject.Rotation) * originToCornerDistance), gameObject.Position.Y + (float)(Math.Cos(MathHelper.ToRadians(45) - gameObject.Rotation) * originToCornerDistance));
            }
            else if (corner == 3)
            {
                cornerPosition = new Vector2(gameObject.Position.X + (float)(Math.Sin(MathHelper.ToRadians(-45) - gameObject.Rotation) * originToCornerDistance), gameObject.Position.Y + (float)(Math.Cos(MathHelper.ToRadians(-45) - gameObject.Rotation) * originToCornerDistance));
            }
            return cornerPosition;
        }

        public static class CollisionTypes
        {
            public static short Static = 0;
            public static short Dynamic = 1;
        }
    }
}