﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace GameInputDemo
{

    public class GameDemo : Game
    {
        GraphicsDeviceManager graphics;
        private GameObject box1;
        private GameObject box2;
        private Color bgcolor = new Color(16, 16, 18);
        public SpriteBatch spriteBatch;

        public GameDemo()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;

        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            box1 = new GameObject(Content.Load<Texture2D>("box1"), new Vector2(150, 250));
            box2 = new GameObject(Content.Load<Texture2D>("box2"), new Vector2(350, 150));
            

            box1.Rotation = MathHelper.ToRadians(45);
            box2.Rotation = MathHelper.ToRadians(45);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            BoxMovement();
            if (box1.IsCollidingWith(box2))
            {
                bgcolor = new Color(12, 34, 56);
            }
            else
            {
                bgcolor = new Color(16, 16, 18);
            }
            base.Update(gameTime);
        }

        
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(bgcolor);

            //spriteBatch.Begin(transformMatrix: camera.GetViewMatrix());
            //spriteBatch.End();

            // Overlay
            spriteBatch.Begin();
            box1.Draw(spriteBatch);
            box2.Draw(spriteBatch);
            spriteBatch.End();


            base.Draw(gameTime);
        }

        private void BoxMovement()
        {
            KeyboardState ks = Keyboard.GetState();
            MouseState ms = Mouse.GetState();

            if (ks.IsKeyDown(Keys.A))
            {
                box1.Position = new Vector2(ms.X, ms.Y);
                if (ks.IsKeyDown(Keys.R))
                {
                    box1.Rotation += MathHelper.ToRadians(2);
                }
                if (ks.IsKeyDown(Keys.E))
                {
                    box1.Rotation -= MathHelper.ToRadians(2);
                }
            }
            else if (ks.IsKeyDown(Keys.B))
            {
                box2.Position = new Vector2(ms.X, ms.Y);
                if (ks.IsKeyDown(Keys.R))
                {
                    box2.Rotation += MathHelper.ToRadians(2);
                }
                if (ks.IsKeyDown(Keys.E))
                {
                    box2.Rotation -= MathHelper.ToRadians(2);
                }
            }
        }
    }
}
