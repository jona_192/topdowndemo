﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace GameInputDemo
{
    class Player : GameObject
    {
        public Player(ContentManager Content) : base(Content.Load<Texture2D>("player"), new Vector2(200,200))
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public void Update()
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();
            TrackTo(new Vector2(mouseState.X, mouseState.Y));

            if (keyboardState.IsKeyDown(Keys.W))
            {
                MoveY(-10);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                MoveY(10);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                MoveX(-10);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                MoveX(10);
            }
        }
    }
}
