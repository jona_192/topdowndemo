﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameInputDemo
{
    public class Sprite
    {
        public readonly Texture2D texture;

        private Vector2 Origin;
        public float Opacity;

        private Vector2 _scale;
        private float _rotation;
        private Vector2 _position;
        
        public Vector2 Scale
        {
            get
            {
                return this._scale;
            }
            set
            {
                this._scale = value;
            }
        }

        public float Rotation
        {
            get
            {
                return this._rotation;
            }
            set
            {
                this._rotation  = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                return this._position;
            }
            set
            {
                this._position = value;
            }
        }


        public Sprite(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.Position = position;
            this.Scale = new Vector2(1, 1);
            this.Rotation = 0;
            this.Origin = new Vector2(texture.Width / 2, texture.Height / 2);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(texture, Position, rotation: Rotation, scale: Scale, origin: Origin, color: Color.White * 0.5f);
        }

        public virtual void MoveX(float position)
        {
            this.Position = new Vector2(this.Position.X + position, this.Position.Y);
        }

        public virtual void MoveY(float position)
        {
            this.Position = new Vector2(this.Position.X, this.Position.Y + position);
        }

        public virtual void Rotate(float angle, bool isRadian = false)
        {
            if (!isRadian)
            {
                angle = MathHelper.ToRadians(angle);
            }
            Rotation += angle;
        }
    }
}
