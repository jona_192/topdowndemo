﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameInputDemo
{
    public class GameObject : Sprite
    {
        public string Tag;
        public bool isVisible = true;
        public CollisionObject CollisionObject;

        public GameObject(Texture2D texture, Vector2 position, string tag = null) : base(texture, position)
        {
            this.Tag = tag;
            CollisionObject = new CollisionObject(this, CollisionObject.CollisionTypes.Dynamic);
        }

        public override void MoveX(float position)
        {
            base.MoveX(position);
        }

        public override void MoveY(float position)
        {
            base.MoveY(position);
        }

        public override void Rotate(float angle, bool isRadian = false)
        {
            base.Rotate(angle, isRadian);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                base.Draw(spriteBatch);
            }
            
        }

        public bool IsCollidingWith(GameObject gameObject)
        {
            float[,] resultPAxis = getMinMaxProjection(this.CollisionObject.Normals[0], this.CollisionObject.CornerPositions, gameObject.CollisionObject.CornerPositions);
            float[,] resultQAxis = getMinMaxProjection(this.CollisionObject.Normals[1], this.CollisionObject.CornerPositions, gameObject.CollisionObject.CornerPositions);

            float[,] resultRAxis = getMinMaxProjection(gameObject.CollisionObject.Normals[0], this.CollisionObject.CornerPositions, gameObject.CollisionObject.CornerPositions);
            float[,] resultSAxis = getMinMaxProjection(gameObject.CollisionObject.Normals[1], this.CollisionObject.CornerPositions, gameObject.CollisionObject.CornerPositions);

            bool separateP = resultPAxis[1, 1] < resultPAxis[0, 0] || resultPAxis[0, 1] < resultPAxis[1, 0];
            bool separateQ = resultQAxis[1, 1] < resultQAxis[0, 0] || resultQAxis[0, 1] < resultQAxis[1, 0];
            bool separateR = resultRAxis[1, 1] < resultRAxis[0, 0] || resultRAxis[0, 1] < resultRAxis[1, 0];
            bool separateS = resultSAxis[1, 1] < resultSAxis[0, 0] || resultSAxis[0, 1] < resultSAxis[1, 0];
            if (separateP || separateQ || separateR || separateS)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private float[,] getMinMaxProjection(Vector2 normal, List<Vector2> ownerCornerPositions, List<Vector2> gameObjectCornerPositions)
        {
            float thisProjectionMin = float.MaxValue;
            float thisProjectionMax = float.MinValue;
            float gameObjectProjectionMin = float.MaxValue;
            float gameObjectProjectionMax = float.MinValue;

            for (int i = 0; i < 4; i++)
            {
                float ownerProjection = Vector2.Dot(ownerCornerPositions[i], normal);
                float gameObjectProjection = Vector2.Dot(gameObjectCornerPositions[i], normal);
                if (ownerProjection > thisProjectionMax)
                {
                    thisProjectionMax = ownerProjection;
                }
                if (gameObjectProjection > gameObjectProjectionMax)
                {
                    gameObjectProjectionMax = gameObjectProjection;
                }

                if (ownerProjection < thisProjectionMin)
                {
                    thisProjectionMin = ownerProjection;
                }
                if (gameObjectProjection < gameObjectProjectionMin)
                {
                    gameObjectProjectionMin = gameObjectProjection;
                }
            }
            return new float[,] { { thisProjectionMin, thisProjectionMax }, { gameObjectProjectionMin, gameObjectProjectionMax } };
        }

        public void TrackTo(Vector2 objectPosition)
        {
            float a = (objectPosition.X - Position.X);
            float g = (objectPosition.Y - Position.Y);
            float rotation = (float)Math.Atan(g / a);
            if (a < 0)
            {
                rotation -= (float)MathHelper.ToRadians(180);
            }
            Rotation = rotation + (float)MathHelper.ToRadians(90);
        }
    }
}
